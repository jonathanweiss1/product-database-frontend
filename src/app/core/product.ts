export class Product {
    articleNo: bigint;
    name: string;
    price: number;

    constructor(articleNo: bigint, name: string, price: number) {
        this.articleNo = articleNo;
        this.name = name;
        this.price = price;
    }
}