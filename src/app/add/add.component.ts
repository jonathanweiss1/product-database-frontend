import { Component, NgModule, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductService } from "../core/services/products.service";
import { Product } from '../core/product';


/**
 * This component allows the user to add a new product to the list.
 */
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent {
  addProductForm: FormGroup;
  successful: boolean;
  responseMessage: string = '';

  constructor(private readonly productService: ProductService) {
  }

  ngOnInit() {
    let fb = new FormBuilder;
    this.addProductForm = fb.group({
      articleNo: null,
      name: null,
      price: null
    });
  }

  /**
   * on submit, send request to server and update successmessage
   */
  async onSubmit() {
    let product = new Product(this.addProductForm.value.articleNo, this.addProductForm.value.name, this.addProductForm.value.price);
    this.productService.addProduct(product).subscribe(response => {
      console.log(response.status);
      this.successful = response.status == 201;
      this.updateResponseMessage();
    }, error => {
      this.successful = false;
      this.updateResponseMessage();
    });
  }

  /**
   * Update responsemessage depending on successful variable
   */
  updateResponseMessage() {
    if (this.successful) {
      this.responseMessage = "Erfolgreich hinzugefügt";
    }
    else {
      this.responseMessage = "Fehler";
    }
    console.log(this.responseMessage);
  }
}
