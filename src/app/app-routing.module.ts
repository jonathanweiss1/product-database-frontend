import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {IntroComponent} from "./intro/intro.component";
import { ProductlistComponent } from './productlist/productlist.component';
import { AddComponent } from './add/add.component';

const routes: Routes = [
  {
    path: '',
    component: IntroComponent
  },
  {
    path: 'list',
    component: ProductlistComponent
  },
  {
    path: 'add',
    component: AddComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
