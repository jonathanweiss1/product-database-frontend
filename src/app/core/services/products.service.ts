import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Product} from "../product"

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  headers:HttpHeaders;

  constructor(private readonly http: HttpClient) { 
    this.headers = new HttpHeaders();
    this.headers = this.headers.set('Content-Type','application/json');
  }

  /**
   * Get list with all products from database
   * @returns 
   */
  getProductList(): Observable<any> {
    return this.http.get('http://localhost:8080/products', {headers: this.headers, observe: 'response'});
  }

/**
 * Delete product with specified articleNo from database
 * @param articleNo 
 * @returns 
 */
  deleteProduct(articleNo:BigInt): Observable<any> {
    return this.http.delete('http://localhost:8080/products/' + articleNo.toString(), {headers: this.headers, observe: 'response'});
  }

  /**
   * Method for adding or updating entries in the database
   * @param product A product object
   * @returns Observable response of request
   */
  addProduct(product:Product): Observable<any> {
    console.log(JSON.stringify(product));
    return this.http.post('http://localhost:8080/products', JSON.stringify(product), {headers: this.headers, observe: 'response'} );
  }
}
