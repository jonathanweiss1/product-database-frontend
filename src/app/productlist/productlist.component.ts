import {Component, OnInit} from '@angular/core';
import { ProductService } from '../core/services/products.service';
import { Product } from '../core/product';

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.css']
})
export class ProductlistComponent implements OnInit{
  productList:Product[];
  errorMessage:string;
  showErrorMessage:boolean;
  loadingMessage:string;
  showLoadingMessage:boolean;
  connectionErrorMessage:string;
  showConnectionErrorMessage:boolean;

  /**
   * Since spring boot is not exposing id fields directly, this function takes them from the link to the resource
   * @param link 
   */
  getIdFromLink(link:String):bigint {
    let slices = link.split('/');
    return BigInt(slices[slices.length - 1]);
  }

  /**
   * Maps an entry returned from the database to a product object and pushes it 
   * @param entry 
   */
  mapEntry(entry:any){
    return new Product(this.getIdFromLink(entry._links.self.href), entry.name, entry.price);
  }

  fillProductList(entries:any[]){
    console.log("entries", entries);
    for(let i = 0; i < entries.length; i++){
      let product = this.mapEntry(entries[i]);
      this.productList.push(product);
    }
  }

  constructor(private readonly productService: ProductService) {
    this.errorMessage = "Artikel konnte nicht entfernt werden";
    this.showErrorMessage = false;
    this.loadingMessage = "Lädt ...";
    this.showLoadingMessage = false;
    this.connectionErrorMessage = "Keine Verbindung zum Server";
    this.showConnectionErrorMessage = false;
  }

  initProductList() {
    this.showLoadingMessage = true;
    this.productList = new Array<Product>;
    this.productService.getProductList().subscribe(response => {
      console.log(response.body._embedded.products);
      this.fillProductList(response.body._embedded.products);
      this.showLoadingMessage = false;
    },
    error => {
      console.log(error);
      this.showLoadingMessage = false;
      this.showConnectionErrorMessage = true;
    });
    console.log(this.productList);
  }

  ngOnInit() {
    this.initProductList();
  }

  /**
   * try to delete the selected product based on articleNo and refresh the list
   * @param product 
   */
  onDelete(product:Product) {
    this.productService.deleteProduct(product.articleNo).subscribe(response => {
      this.showErrorMessage = false;
      this.initProductList();
    },
    error => {
      this.showErrorMessage = true;
      console.log(error);
    })
  }
}
